import os


PROJECT_DIR = os.getcwd()
DOTENV_FILE = '.env.example'


def bootstrap_dotenv_file():
    """Bootstrap the generated example .env file."""
    print(f'\nBootstrapping {DOTENV_FILE}...')
    with open(DOTENV_FILE) as f:
        content = f.read()
    # replace tag by install path
    content = content.replace('$((PROJECT_DIR))', PROJECT_DIR)
    # replace file content
    with open(DOTENV_FILE, 'w') as f:
        f.write(content)
    print(f'Bootstrapping {DOTENV_FILE} completed!\n')


if __name__ == '__main__':
    bootstrap_dotenv_file()
