"""Commands module."""
from subprocess import call

import click


@click.command()
@click.option('--watch', is_flag=True)
@click.argument('args', nargs=-1)
def test(watch, args):
    """Run unit tests."""
    if watch:
        call(['ptw'] + list(args))
    else:
        rv = call(['pytest'] + list(args))
        exit(rv)


@click.command()
def lint():
    """Run the flake8 linter."""
    rv = call(['flake8', '{{cookiecutter.app_name}}/', 'tests/'])
    exit(rv)
