"""Application settings."""
from environs import Env

env = Env()
env.read_env()


with env.prefixed('{{cookiecutter.app_name|upper}}_'):
    DEBUG = env.bool('DEBUG')
    ENV = env.str('ENV')
    SECRET_KEY = env.str('SECRET_KEY')

    SQLALCHEMY_DATABASE_URI = env.str('SQLALCHEMY_DATABASE_URI')
    SQLALCHEMY_TRACK_MODIFICATIONS = env.bool('SQLALCHEMY_TRACK_MODIFICATIONS')

    APISPEC_TITLE = env.str('APISPEC_TITLE')
    APISPEC_VERSION = env.str('APISPEC_VERSION')
    APISPEC_OAS_VERSION = env.str('APISPEC_OAS_VERSION')
