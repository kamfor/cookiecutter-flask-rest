"""Database extension."""
from functools import wraps

from flask import abort
from flask_sqlalchemy import BaseQuery, SQLAlchemy


class CustomQuery(BaseQuery):
    """Override Flask-SQLAlchemy's BaseQuery class with custom methods."""

    def get_or_404(self, ident, description=None):
        """Like get but aborts with 404 and a custom description."""
        rv = self.get(ident)
        if rv is None:
            abort(404, description)
        return rv

    def first_or_404(self, description=None):
        """Like first but aborts with 404 and a custom description."""

        rv = self.first()
        if rv is None:
            abort(404, description)
        return rv


db = SQLAlchemy(query_class=CustomQuery)


class CRUDMixin(object):
    """Mixin that adds convenience methods for CRUD operations."""

    @classmethod
    def create(cls, **kwargs):
        """Create a new record and save it the database."""
        instance = cls(**kwargs)
        return instance.save()

    def update(self, commit=False, **kwargs):
        """Update specific fields of a record."""
        for attr, value in kwargs.items():
            setattr(self, attr, value)
        return commit and self.save() or self

    def save(self, commit=False):
        """Save the record."""
        db.session.add(self)
        if commit:
            db.session.commit()
        return self

    def delete(self, commit=False):
        """Remove the record from the database."""
        db.session.delete(self)
        return commit and db.session.commit()


class Model(CRUDMixin, db.Model):
    """Base model class that includes CRUD convenience methods."""

    __abstract__ = True


def commit_session(view_func):
    """Request decorator to commit database session ."""

    @wraps(view_func)
    def commit_wrapper(*args, **kwargs):
        """Commit database session after view function call."""
        response = view_func(*args, **kwargs)
        db.session.commit()
        return response

    return commit_wrapper
