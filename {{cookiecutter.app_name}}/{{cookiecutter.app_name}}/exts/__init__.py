"""Application exts."""
from flask_apispec import FlaskApiSpec
from flask_bcrypt import Bcrypt
from flask_marshmallow import Marshmallow
from flask_migrate import Migrate

from .database import db  # noqa

bcrypt = Bcrypt()
ma = Marshmallow()
migrate = Migrate()
specs = FlaskApiSpec()
