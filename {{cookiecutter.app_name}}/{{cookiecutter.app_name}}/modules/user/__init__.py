"""The user module."""
from .models import Role, User  # noqa
from .schemas import RoleSchema, UserSchema  # noqa
from .views import bp, register_specs  # noqa
