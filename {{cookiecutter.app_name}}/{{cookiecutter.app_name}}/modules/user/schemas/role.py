"""Role schema."""
from {{cookiecutter.app_name}}.exts import ma


class RoleSchema(ma.Schema):
    """Role model schema."""

    class Meta:  # noqa
        strict = True

    id = ma.Int(dump_only=True)
    name = ma.Str(required=True)
    users = ma.Nested('NestedUserSchema', dump_only=True, many=True)


class NestedRoleSchema(RoleSchema):
    """Role model schema excluding `users` fields."""

    class Meta:  # noqa
        strict = True
        exclude = ('users',)
