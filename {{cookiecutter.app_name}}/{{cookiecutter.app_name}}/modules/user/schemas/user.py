"""User schema."""
from {{cookiecutter.app_name}}.exts import ma


class UserSchema(ma.Schema):
    """User model schema."""

    class Meta:  # noqa
        strict = True

    id = ma.Int(dump_only=True)
    name = ma.Str(required=True)
    email = ma.Str(required=True)
    password = ma.Str(load_only=True)
    roles = ma.Nested('NestedRoleSchema', dump_only=True, many=True, exclude=('users',))


class NestedUserSchema(UserSchema):
    """User model schema excluding `roles` fields."""

    class Meta:  # noqa
        strict = True
        exclude = ('roles',)
