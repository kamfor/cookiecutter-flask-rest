"""User views."""
from flask import make_response
from flask_apispec import MethodResource, doc, marshal_with, use_kwargs
from webargs import fields

from {{cookiecutter.app_name}}.exts.database import commit_session
from {{cookiecutter.app_name}}.modules.user.models import Role, User
from {{cookiecutter.app_name}}.modules.user.schemas import UserSchema


@doc(tags=['Users'])
class UserList(MethodResource):
    """User list view."""

    @doc(summary='Get list of users.')
    @marshal_with(UserSchema(many=True), code=200)
    def get(self):
        """Get list of users."""
        users = User.query.all()
        return users, 200

    @doc(summary='Create a user.')
    @use_kwargs(UserSchema)
    @marshal_with(UserSchema, code=201)
    @commit_session
    def post(self, **user_args):
        """Create a user."""
        user = User.create(**user_args)
        return user, 201


@doc(tags=['Users'])
class UserDetail(MethodResource):
    """User detail view."""

    @doc(summary='Get user by id.')
    @marshal_with(UserSchema, code=200)
    def get(self, user_id):
        """Get user by id."""
        user = User.query.get_or_404(user_id, 'User not found.')
        return user, 200

    @doc(summary='Delete user by id.')
    @marshal_with(None, code=204)
    @commit_session
    def delete(self, user_id):
        """Delete user by id."""
        user = User.query.get_or_404(user_id, 'User not found.')
        user.delete()
        return make_response('', 204)

    @doc(summary='Update user by id.')
    @use_kwargs(UserSchema)
    @marshal_with(UserSchema, code=200)
    @commit_session
    def put(self, user_id, **user_args):
        """Update user by id."""
        user = User.query.get_or_404(user_id, 'User not found.')
        user.update(**user_args)
        return user, 200


@doc(summary='Add role to user.', tags=['Users'])
@use_kwargs({'role_id': fields.Int()})
@marshal_with(None, code=204)
@commit_session
def add_user_role(user_id, role_id):
    """Add role to user."""
    user = User.query.get_or_404(user_id, 'User not found.')
    role = Role.query.get_or_404(role_id, 'Role not found.')
    user.roles.append(role)
    user.save()
    return make_response('', 204)


@doc(summary='Remove role from user.', tags=['Users'])
@use_kwargs({'role_id': fields.Int()})
@marshal_with(None, code=204)
@commit_session
def remove_user_role(user_id, role_id):
    """Remove role from user."""
    user = User.query.get_or_404(user_id, 'User not found.')
    user.roles = [role for role in user.roles if role.id != role_id]
    user.save()
    return make_response('', 204)
