"""Role views."""
from flask import make_response
from flask_apispec import MethodResource, doc, marshal_with, use_kwargs

from {{cookiecutter.app_name}}.exts.database import commit_session
from {{cookiecutter.app_name}}.modules.user.models import Role
from {{cookiecutter.app_name}}.modules.user.schemas import RoleSchema


@doc(tags=['Roles'])
class RoleList(MethodResource):
    """Role list view."""

    @doc(summary='Get list of roles.')
    @marshal_with(RoleSchema(many=True), code=200)
    def get(self):
        """Get list of roles."""
        roles = Role.query.all()
        return roles, 200

    @doc(summary='Create a role.')
    @use_kwargs(RoleSchema)
    @marshal_with(RoleSchema, code=201)
    @commit_session
    def post(self, **role_args):
        """Create a role."""
        role = Role.create(**role_args)
        return role, 201


@doc(tags=['Roles'])
class RoleDetail(MethodResource):
    """Role detail view."""

    @doc(summary='Get role by id.')
    @marshal_with(RoleSchema, code=200)
    def get(self, role_id):
        """Get role by id."""
        role = Role.query.get_or_404(role_id, 'Role not found.')
        return role, 200

    @doc(summary='Delete role by id.')
    @marshal_with(None, code=204)
    @commit_session
    def delete(self, role_id):
        """Delete role by id."""
        role = Role.query.get_or_404(role_id, 'Role not found.')
        role.delete()
        return make_response('', 204)

    @doc(summary='Update role by id.')
    @use_kwargs(RoleSchema)
    @marshal_with(RoleSchema, code=200)
    @commit_session
    def put(self, role_id, **role_args):
        """Update role by id."""
        role = Role.query.get_or_404(role_id, 'Role not found.')
        role.update(**role_args)
        return role, 200
