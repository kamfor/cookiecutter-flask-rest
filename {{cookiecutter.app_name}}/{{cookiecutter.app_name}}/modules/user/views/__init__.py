"""User related views."""
from flask import Blueprint
from flask_apispec import FlaskApiSpec

from .role import RoleDetail, RoleList
from .user import UserDetail, UserList, add_user_role, remove_user_role

bp = Blueprint('user', __name__, url_prefix='/users')

# User urls
bp.add_url_rule('/', view_func=UserList.as_view('userlist'))
bp.add_url_rule('/<int:user_id>', view_func=UserDetail.as_view('userdetail'))
bp.add_url_rule(
    '/<int:user_id>/roles',
    view_func=add_user_role,
    methods=['POST'],
    provide_automatic_options=False,
)
bp.add_url_rule(
    '/<int:user_id>/roles',
    view_func=remove_user_role,
    methods=['DELETE'],
    provide_automatic_options=False,
)

# Role urls
bp.add_url_rule('/roles', view_func=RoleList.as_view('rolelist'))
bp.add_url_rule('/roles/<int:role_id>', view_func=RoleDetail.as_view('roledetail'))


def register_specs(specs: FlaskApiSpec):
    """Register user related swagger specifications."""
    specs.register(UserList, blueprint=bp.name, endpoint='userlist')
    specs.register(UserDetail, blueprint=bp.name, endpoint='userdetail')
    specs.register(add_user_role, blueprint=bp.name)
    specs.register(remove_user_role, blueprint=bp.name)

    specs.register(RoleList, blueprint=bp.name, endpoint='rolelist')
    specs.register(RoleDetail, blueprint=bp.name, endpoint='roledetail')
