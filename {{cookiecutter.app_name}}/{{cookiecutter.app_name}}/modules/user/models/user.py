"""User model."""
from {{cookiecutter.app_name}}.exts import bcrypt
from {{cookiecutter.app_name}}.exts.database import Model, db

# Many-to-Many association table between the user and role tables
user_role_association = db.Table(
    'user_role_association',
    db.metadata,
    db.Column('user', db.Integer, db.ForeignKey('user.id')),
    db.Column('role', db.Integer, db.ForeignKey('role.id')),
)


class User(Model):
    """User model."""

    __tablename__ = 'user'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, nullable=False)
    email = db.Column(db.String, nullable=False)
    password = db.Column(db.LargeBinary(128), nullable=True)

    roles = db.relationship(
        'Role', secondary='user_role_association', back_populates='users'
    )

    def __init__(self, *args, **kwargs):
        """User constructor."""
        self.set_password(kwargs.pop('password', None))
        super().__init__(*args, **kwargs)

    def set_password(self, password):
        """Hash and set password."""
        if password:
            self.password = bcrypt.generate_password_hash(password)

    def check_password(self, password):
        """Check password."""
        return bcrypt.check_password_hash(self.password, password)

    def update(self, **kwargs):
        """User update."""
        password = kwargs.pop('password', None)
        if password:
            self.set_password(password)
        super().update(**kwargs)

    def __repr__(self):
        """User representation."""
        return f'<User({self.id})>'
