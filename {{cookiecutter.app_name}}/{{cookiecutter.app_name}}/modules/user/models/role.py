"""Role model."""
from {{cookiecutter.app_name}}.exts.database import Model, db


class Role(Model):
    """Role model."""

    __tablename__ = 'role'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, nullable=False)

    users = db.relationship(
        'User', secondary='user_role_association', back_populates='roles'
    )

    def __repr__(self):
        """Role representation."""
        return f'<Role({self.id})>'
