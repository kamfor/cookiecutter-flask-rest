"""App factory module."""
from flask import Flask, jsonify
from flask_apispec import FlaskApiSpec

from {{cookiecutter.app_name}} import commands, exts, modules


def create_app(config_path='{{cookiecutter.app_name}}.settings'):
    """Flask application factory function."""
    app = Flask(__name__.split('.')[0])
    app.url_map.strict_slashes = False
    app.config.from_object(config_path)

    register_blueprints(app)
    register_extensions(app)
    register_errorhandlers(app)
    register_shellcontext(app)
    register_commands(app)

    return app


def register_extensions(app: Flask):
    """Register extensions on the app instance."""

    def register_specs(app: Flask, specs: FlaskApiSpec):
        """Register swagger specs."""
        specs.init_app(app)
        modules.user.register_specs(specs)

    exts.bcrypt.init_app(app)
    exts.db.init_app(app)
    exts.migrate.init_app(app, exts.db)
    exts.ma.init_app(app)
    register_specs(app, exts.specs)


def register_blueprints(app: Flask):
    """Register blueprints on the app instance."""
    app.register_blueprint(modules.user.bp)


def register_errorhandlers(app: Flask):
    """Register error handlers."""

    def render_error(error):
        """Render error message."""
        response = {'message': error.description}

        # check for additional webargs error messages
        if hasattr(error, 'exc') and hasattr(error.exc, 'messages'):
            response = {'message': error.exc.messages}

        return jsonify(response), error.code

    for errcode in [401, 403, 404, 405, 422, 500]:
        app.register_error_handler(errcode, render_error)


def register_shellcontext(app: Flask):
    """Register shell context on the app instance."""

    def shell_context():
        """Shell context objects."""
        return {'db': exts.db, 'User': modules.user.User, 'Role': modules.user.Role}

    app.shell_context_processor(shell_context)


def register_commands(app: Flask):
    """Register custom cli commands."""
    app.cli.add_command(commands.lint)
    app.cli.add_command(commands.test)
