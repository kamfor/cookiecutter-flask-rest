# {{ cookiecutter.project_name }}

{{ cookiecutter.project_short_description }}

# Quickstart

Create and activate a virtual environment and run the following commands to bootstrap your environment

    pip install -r requirements-dev.txt
    cp .env.example .env

To create your initial database migration and tables run the following

    flask db init
    flask db migrate
    flask db upgrade

After you can run the application as usual

    flask run

By default the app serves a [Swagger UI](https://swagger.io/tools/swagger-ui/) at http://localhost:5000/swagger-ui so feel free to explore the API using the UI.

# Shell

To open the interactive shell, run

    flask shell

By default, you will have access to the Flask `app` and additionally the shell is configured to provide access to the `db`, `User` and `Role` objects.

# Custom commands

To run lint checks execute

    flask lint

To run all tests using

    flask test

Tests can also be executed in watch mode using

    flask test --watch

You can also pass additional arguments to `pytest` and `pytest-watch` using the `--` end-of-options argument, for example to run test suits of a single test file

	flask test -- tests/test_role_views.py

# Migrations

Whenever a database migration needs to be made (eg. . Run the following commands

    flask db migrate

This will generate a new migration script. Then run

    flask db upgrade

To apply the migration.

For a full migration command reference, run  `flask db --help`.