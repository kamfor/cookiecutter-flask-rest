"""Defines fixtures available to all tests."""

import pytest

from {{cookiecutter.app_name}}.app import create_app
from {{cookiecutter.app_name}}.exts.database import db as _db

from .factories import RoleFactory, UserFactory


@pytest.fixture
def app():
    """Application for the tests."""
    _app = create_app('tests.settings')
    ctx = _app.test_request_context()
    ctx.push()

    yield _app

    ctx.pop()


@pytest.fixture
def client(app):
    """Application test client."""
    return app.test_client()


@pytest.fixture
def db(app):
    """Database for the tests."""
    _db.app = app
    with app.app_context():
        _db.create_all()

    yield _db

    _db.session.close()
    _db.drop_all()


@pytest.fixture
def user(db):
    """User for the tests."""
    user = UserFactory()
    db.session.commit()
    return user


@pytest.fixture
def role(db):
    """Role for the tests."""
    role = RoleFactory()
    db.session.commit()
    return role
