"""Test settings."""
from environs import Env

env = Env()
env.read_env()

ENV = 'development'
DEBUG = True
SECRET_KEY = 'test-secret'
BCRYPT_LOG_ROUNDS = 4
TESTING = True
SQLALCHEMY_TRACK_MODIFICATIONS = False

with env.prefixed('TEST_'):
    SQLALCHEMY_DATABASE_URI = env.str('SQLALCHEMY_DATABASE_URI')
