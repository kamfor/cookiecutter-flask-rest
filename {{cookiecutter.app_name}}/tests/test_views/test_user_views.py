"""User view unit tests."""
import pytest
from flask import url_for

from {{cookiecutter.app_name}}.modules.user.models import User

DETAIL_ENDPOINT = 'user.userdetail'
LIST_ENDPOINT = 'user.userlist'


def url_for_user_detail(user_id=None):
    """Get user detail endpoint."""
    return url_for(DETAIL_ENDPOINT, user_id=user_id)


def url_for_user_list():
    """Get user list endpoint."""
    return url_for(LIST_ENDPOINT)


class TestUserDetail:
    """User detail view tests."""

    def test_get_user_by_id(self, client, user):
        """Test get user by id."""
        res = client.get(url_for_user_detail(user_id=user.id))
        json_data = res.get_json()

        user = User.query.get(json_data['id'])

        assert res.status_code == 200
        assert json_data['name'] == user.name
        assert json_data['email'] == user.email
        assert 'roles' in json_data

    def test_delete_user_by_id(self, client, user):
        """Test delete user by id."""
        res = client.delete(url_for_user_detail(user_id=user.id))

        assert res.status_code == 204
        assert User.query.get(user.id) is None

    def test_put_user_by_id(self, client, user):
        """Test put user by id."""
        user_data = {
            'name': 'test user',
            'email': 'test@bar.com',
            'password': 'test_pass',
        }
        res = client.put(url_for_user_detail(user_id=user.id), json=user_data)
        json_data = res.get_json()

        updated_user = User.query.get(json_data['id'])

        assert res.status_code == 200
        assert updated_user.name == user_data['name']
        assert updated_user.email == user_data['email']
        assert updated_user.check_password(user_data['password'])


@pytest.mark.usefixtures('db')
class TestUserList:
    """User list view tests."""

    def test_get_users(self, client, user):
        """Test get user list."""
        res = client.get(url_for_user_list())
        json_data = res.get_json()

        assert res.status_code == 200
        assert len(json_data) == 1
        assert json_data[0]['id'] == user.id
        assert json_data[0]['name'] == user.name
        assert json_data[0]['email'] == user.email
        assert 'roles' in json_data[0]

    def test_create_user(self, client):
        """Test create user."""
        user_data = {
            'name': 'test user',
            'email': 'test@bar.com',
            'password': 'testpass',
        }
        res = client.post(url_for_user_list(), json=user_data)
        json_data = res.get_json()

        user = User.query.get(json_data['id'])
        assert res.status_code == 201
        assert user.name == user_data['name']
        assert user.email == user_data['email']
        assert user.check_password(user_data['password'])
