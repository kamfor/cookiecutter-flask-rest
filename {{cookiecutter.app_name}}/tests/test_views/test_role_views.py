"""Role view unit tests."""
import pytest
from flask import url_for

from {{cookiecutter.app_name}}.modules.user.models import Role

DETAIL_ENDPOINT = 'user.roledetail'
LIST_ENDPOINT = 'user.rolelist'


def url_for_role_detail(role_id=None):
    """Get role detail endpoint."""
    return url_for(DETAIL_ENDPOINT, role_id=role_id)


def url_for_role_list():
    """Get role list endpoint."""
    return url_for(LIST_ENDPOINT)


class TestRoleDetail:
    """Role detail view tests."""

    def test_get_role_by_id(self, client, role):
        """Test get role by id."""
        res = client.get(url_for_role_detail(role_id=role.id))
        json_data = res.get_json()

        role = Role.query.get(json_data['id'])

        assert res.status_code == 200
        assert json_data['name'] == role.name
        assert 'users' in json_data

    def test_delete_role_by_id(self, client, role):
        """Test delete role by id."""
        res = client.delete(url_for_role_detail(role_id=role.id))

        assert res.status_code == 204
        assert Role.query.get(role.id) is None

    def test_put_role_by_id(self, client, role):
        """Test put role by id."""
        role_data = {'name': 'test role'}
        res = client.put(url_for_role_detail(role_id=role.id), json=role_data)
        json_data = res.get_json()

        updated_role = Role.query.get(json_data['id'])

        assert res.status_code == 200
        assert updated_role.name == role_data['name']


@pytest.mark.usefixtures('db')
class TestRoleList:
    """Role list view tests."""

    def test_get_roles(self, client, role):
        """Test get user list."""
        res = client.get(url_for_role_list())
        json_data = res.get_json()

        assert res.status_code == 200
        assert len(json_data) == 1
        assert json_data[0]['id'] == role.id
        assert json_data[0]['name'] == role.name
        assert 'users' in json_data[0]

    def test_create_role(self, client):
        """Test create role."""
        role_data = {'name': 'test role'}
        res = client.post(url_for_role_list(), json=role_data)
        json_data = res.get_json()

        role = Role.query.get(json_data['id'])
        assert res.status_code == 201
        assert role.name == role_data['name']
