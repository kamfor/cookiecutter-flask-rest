"""User model unit tests."""
import pytest

from {{cookiecutter.app_name}}.modules.user.models import User
from tests.factories import RoleFactory, UserFactory


@pytest.mark.usefixtures('db')
class TestUser:
    """User tests."""

    def test_factory(self, db):
        """Test user factory."""
        user = UserFactory()
        db.session.commit()
        assert bool(user.name)
        assert bool(user.email)
        assert bool(user.password)
        assert user.check_password('example')

    def test_roles(self):
        """Add a user to many roles."""
        user = User(name='user', email='user@bar.com')
        user.save(commit=True)

        role1 = RoleFactory()
        role1.users.append(user)
        role1.save(commit=True)

        role2 = RoleFactory()
        role2.users.append(user)
        role2.save(commit=True)

        assert user.roles == [role1, role2]
