"""Role model unit tests."""
import pytest

from {{cookiecutter.app_name}}.modules.user.models import Role
from tests.factories import RoleFactory, UserFactory


@pytest.mark.usefixtures('db')
class TestRole:
    """Role tests."""

    def test_factory(self, db):
        """Test role factory."""
        role = RoleFactory()
        db.session.commit()
        assert bool(role.name)

    def test_users(self):
        """Add a role to many users."""
        role = Role(name='admin')
        role.save(commit=True)

        user1 = UserFactory()
        user1.roles.append(role)
        user1.save(commit=True)

        user2 = UserFactory()
        user2.roles.append(role)
        user2.save(commit=True)

        assert role.users == [user1, user2]
