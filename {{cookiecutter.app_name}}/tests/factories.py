"""Factories to help in tests."""
from factory import Sequence
from factory.alchemy import SQLAlchemyModelFactory

from {{cookiecutter.app_name}}.exts.database import db
from {{cookiecutter.app_name}}.modules.user.models import Role, User


class BaseFactory(SQLAlchemyModelFactory):
    """Base factory."""

    class Meta:  # noqa

        abstract = True
        sqlalchemy_session = db.session


class UserFactory(BaseFactory):
    """User factory."""

    name = Sequence(lambda n: 'user{0}'.format(n))
    email = Sequence(lambda n: 'user{0}@example.com'.format(n))
    password = 'example'

    class Meta:  # noqa

        model = User


class RoleFactory(BaseFactory):
    """Role factory."""

    name = Sequence(lambda n: 'role{0}'.format(n))

    class Meta:  # noqa

        model = Role
