"""Invoke tasks."""
import os
import json
import shutil
import webbrowser

from invoke import task

from cookiecutter.main import cookiecutter

HERE = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(HERE, 'cookiecutter.json'), 'r') as fp:
    COOKIECUTTER_SETTINGS = json.load(fp)
# Match default value of app_name from cookiecutter.json
COOKIE = os.path.join(
    HERE,
    COOKIECUTTER_SETTINGS['project_name'].lower().replace(' ', '_').replace('-', '_'),
)
REQUIREMENTS = os.path.join(COOKIE, 'requirements-dev.txt')


@task
def build(ctx, packaging='requirements'):
    """Build the cookiecutter."""
    cookiecutter('.', no_input=True)
    shutil.copyfile(os.path.join(COOKIE, '.env.example'), os.path.join(COOKIE, '.env'))


@task
def clean(ctx):
    """Clean out generated cookiecutter."""
    if os.path.exists(COOKIE):
        shutil.rmtree(COOKIE)
        print('Removed {0}'.format(COOKIE))
    else:
        print('App directory does not exist. Skipping.')


def _run_flask_command(ctx, command):
    os.chdir(COOKIE)
    ctx.run('flask {0}'.format(command), echo=True)


@task(pre=[clean, build])
def test(ctx):
    """Run lint commands and tests."""
    ctx.run('pip install -r {0} --ignore-installed'.format(REQUIREMENTS), echo=True)
    _run_flask_command(ctx, 'lint')
    _run_flask_command(ctx, 'test')
