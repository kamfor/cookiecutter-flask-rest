# cookiecutter-flask-rest

Cookiecutter template to quickly bootstrap a REST service built with Flask.

# Quickstart

    $ pip install cookiecutter
    $ cookiecutter https://gitlab.com/kamfor/cookiecutter-flask-rest.git

You will be asked about your basic info (project name, app name, description). This info will be used in your new project.

After successful cookiecutting follow the instructions in the `README.md` of the generated application. 

# Features

- Flask-SQLAlchemy to manage database models 
- Flask-Migrate to manage database migrations
- Flask-Bcrypt for password hashing
- Flask's Click CLI configured with simple commands
- Flask-Apispec to provide Swagger UI 
- pytest and Factory-Boy for testing (example tests included)
- Configuration in environment variables, as per [The Twelve-Factor App](https://12factor.net/config)
- Utilized best practices: [Blueprint](http://flask.pocoo.org/docs/blueprints/) and [Application Factory](http://flask.pocoo.org/docs/patterns/appfactories/) patterns

# License

MIT licensed.

# Changelog

## 1.0.0 (2019-05-01)
- Base template
- Database models and relationships - User, Role
- User and Role API endpoints
- Swagger UI for API browsing